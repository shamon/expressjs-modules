var express = require('express');
var router = express.Router();

/*--custom modules --*/

var fooModule = require('foo');

var barModule = require('bar');

router.get('/foo', function(req, res, next) {

  res.render('index', { title: fooModule.foofunc()/*call the module function*/ });
});

router.get('/bar', function(req, res, next) {

  //nested calls
  console.log(barModule.hello.helloFunc());

  res.render('index', { title: barModule.barFunc()/*call the module function*/ });
});




module.exports = router;
